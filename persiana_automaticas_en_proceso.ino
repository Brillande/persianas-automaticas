/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
*/
int led_rojo = 10;
int led_amarillo = 12;
int sensor_luz = 0;
int persiana = 0; // 0 abajo 1 arriba.

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(led_rojo, OUTPUT);
  pinMode(led_amarillo, OUTPUT);
  Serial.begin(9600);
}

// the loop function runs over and over again forever
void loop() {
  

Serial.begin(9600);
sensor_luz = analogRead(A0);

if (sensor_luz < 200 && persiana == 1){
   digitalWrite(led_rojo, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(led_rojo, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);
  persiana = 0;

}
  else if (sensor_luz > 800 && persiana == 0){
  digitalWrite(led_amarillo, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(led_amarillo, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);
  }

}
